package com.amdocs.ms.config;

import java.util.Collection;

import org.springframework.boot.actuate.endpoint.PublicMetrics;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.spring.boot.SpringBootMetricsCollector;

@Configuration
@ConditionalOnClass(CollectorRegistry.class)
public class PrometheusConfiguration {

	@Bean
	@ConditionalOnMissingBean
	CollectorRegistry metricRegistry() {
		System.out.println("CollectorRegistry intializes..");
		return CollectorRegistry.defaultRegistry;
	}

	@Bean
	ServletRegistrationBean registerPrometheusExporterServlet(CollectorRegistry metricRegistry) {
		System.out.println("Registering prometheus endpoint..");
		return new ServletRegistrationBean(new MetricsServlet(metricRegistry), "/prometheus");
	}

	@Bean
	@ConditionalOnMissingBean(SpringBootMetricsCollector.class)
	public SpringBootMetricsCollector springBootMetricsCollector(Collection<PublicMetrics> publicMetrics) {
		System.out.println("springBootMetricsCollector initializes..");

		SpringBootMetricsCollector springBootMetricsCollector = new SpringBootMetricsCollector(publicMetrics);
		
		springBootMetricsCollector.register();
		return springBootMetricsCollector;
	}


}