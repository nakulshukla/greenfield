package com.amdocs.opensource.poc.prometheus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPromExample2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPromExample2Application.class, args);
	}
}
