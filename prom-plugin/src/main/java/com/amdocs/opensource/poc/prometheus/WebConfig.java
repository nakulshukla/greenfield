package com.amdocs.opensource.poc.prometheus;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="com.amdocs.opensource")
public class WebConfig extends WebMvcConfigurerAdapter {
	
	WebConfig()
	{
		System.out.println("Init here");
	}

	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		System.out.println("Registering interceptors..");
		registry.addInterceptor(new PromRequestInterceptor());
		registry.addInterceptor(new PromTimingInterceptor());
	}
}
