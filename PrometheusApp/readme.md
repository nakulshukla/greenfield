PrometheusApp -
-------------

This project shows how Prometheus instrumentation can be done in a Spring boot project.

Multiple REST endpoints are exposed in this Spring Boot application and performance matrics are scrapped by Prometheus. These metrics are available at  http://localhost:8080/prometheus.


How to run (through CLI)-


mvn clean spring-boot:run
