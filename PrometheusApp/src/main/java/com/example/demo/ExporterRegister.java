package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.prometheus.client.Collector;
import io.prometheus.client.hotspot.MemoryPoolsExports;
import io.prometheus.client.hotspot.StandardExports;

/**
 * Metric exporter register bean to register a list of exporters to the default
 * registry
 */
//@Configuration
public class ExporterRegister {

	private List<Collector> collectors;

	public ExporterRegister(List<Collector> collectors) {
		for (Collector collector : collectors) {
			collector.register();
		}
		this.collectors = collectors;
	}

	public List<Collector> getCollectors() {
		return collectors;
	}

	@Bean
	ExporterRegister exporterRegister() {
		List<Collector> collectors = new ArrayList<>();
		collectors.add(new StandardExports());
		collectors.add(new MemoryPoolsExports());
		ExporterRegister register = new ExporterRegister(collectors);
		return register;
	}

}