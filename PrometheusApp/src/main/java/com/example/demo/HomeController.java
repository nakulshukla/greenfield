package com.example.demo;

import java.time.LocalDateTime;

import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping("/endpointA")
	public String handlerA() throws InterruptedException {
		logger.info("/endpointA");
		Thread.sleep(RandomUtils.nextInt(100));
		return "Just took some time ..";
	}

	@RequestMapping("/endpointB")
	public String handlerB() throws InterruptedException {
		logger.info("/endpointB");
		Thread.sleep(RandomUtils.nextInt(1000));
		return "Took some more time ..";
	}

	@GetMapping("/greet")
	String greet(@RequestParam(defaultValue = "World") String name) {
		return "Hello: " + name + " , the time right now is " + LocalDateTime.now();
	}
	
	@GetMapping("/helloworld")
	String helloWorld(@RequestParam(defaultValue = "World") int count) {
		String msg = "Hello World!!";
		for(int i=0;i<count;i++)
		{
			msg = msg +" "+ msg +"\n";
		}
		
		return msg;
	}

	@GetMapping("/sayHi")
	String greetHi(@RequestParam(defaultValue = "World") String name) {
		return "Hi: " + name + " , the time right now is " + LocalDateTime.now();
	}
}