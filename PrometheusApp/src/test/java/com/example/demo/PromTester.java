package com.example.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PromTester {
	public static void main(String[] args) throws InterruptedException {

		try {

			URL url = null;

			long startTime = System.currentTimeMillis(); // fetch starting time

			BufferedReader br;
			HttpURLConnection conn;

			int counter = 0;

			while (false || (System.currentTimeMillis() - startTime) < 10000) {
				String urlStr = "http://10.235.72.47:8443/helloworld?count=";
				counter++;

				/*
				 * if (counter % 2 == 0) { Thread.sleep(1000); }
				 */

				urlStr = urlStr + counter;

				System.out.println(urlStr);
				url = new URL(urlStr);

				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}

				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
				}

				conn.disconnect();
			}

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}
}